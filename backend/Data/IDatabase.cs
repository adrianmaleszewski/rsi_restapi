using System.Collections.Generic;
using RSI_API.Models;

namespace RSI_API.Data
{
    public interface IDatabase
    {
        List<Article> GetArticles();
    }
}