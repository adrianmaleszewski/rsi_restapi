using System.Collections.Generic;
using RSI_API.Models;

namespace RSI_API.Data
{
    public class Database : IDatabase
    {
        private List<Article> _Articles;

        public Database() {
            _Articles = new List<Article>() {
            new Article() {
                Id=1,
                Name="Wódka",
                Price= 20,
                Quantity =10
            }
            };
        }

        public List<Article> GetArticles() {
            return _Articles;
        }
    }
}