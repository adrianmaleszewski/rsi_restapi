using System.Collections.Generic;
using System.Threading.Tasks;
using RSI_API.Models;

namespace RSI_API.Repositories
{
    public interface IArticleRepository
    {
        void AddArticle(Article Article);
        void UpdateArticle(Article newArticle);
        bool DeleteArticle(int id);
        bool BuyArticle(int id);
        Article GetArticleById(int? id);
        List<Article> GetArticles();
    }
}