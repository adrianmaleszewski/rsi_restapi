using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using RSI_API.Models;
using RSI_API.Data;

namespace RSI_API.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private List<Article> _articles;

        public ArticleRepository(IDatabase db) {
            _articles = db.GetArticles();
        }

        public void AddArticle(Article article) {
            int id = 1;
            
            if (_articles.Count != 0)
                id = _articles.Max(x => x.Id) + 1;

            if (id == 0)
                id = 1;

            article.Id = id;

            _articles.Add(article);

        }
        public void UpdateArticle(Article newArticle) {
            Article article = _articles.FirstOrDefault(x=> x.Id == newArticle.Id);

            if (article != null) {
                article.Name = newArticle.Name;
                article.Price = newArticle.Price;
                article.Quantity = newArticle.Quantity;
            }
        }
        public bool DeleteArticle(int id) {
            Article article = _articles.FirstOrDefault(x => x.Id == id);

            return _articles.Remove(article);
        }
        public Article GetArticleById(int? id) {
            Article article = _articles.FirstOrDefault(x => x.Id == id);

            return article;
        }

        public bool BuyArticle(int id) {
            Article article = _articles.FirstOrDefault(x=> x.Id == id);

            if (article.Quantity > 0) {
                article.Quantity -= 1;

                return true;
            }
            else {
                return false;
            }
        }

        public List<Article> GetArticles() {
            return _articles;
        }
    }
}