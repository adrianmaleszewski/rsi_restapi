using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RSI_API.Models;
using RSI_API.Repositories;

namespace RSI_API.Controllers
{
    [Route("api/[controller]")]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticleRepository _repository;
        public ArticlesController(IArticleRepository ArticleRepository) {
            _repository = ArticleRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Article>> Get() {
            return _repository.GetArticles();
        }

        [HttpGet("{id}")]
        public ActionResult<Article> Get(int id) {
            return _repository.GetArticleById(id);
        }

        [HttpGet("buy/{id}")]
        public ActionResult Buy(int id) {
            bool result = _repository.BuyArticle(id);

            if (result)
                return Ok(_repository.GetArticles());
            else
                return BadRequest("Nie można zakupić produktu. Brak na stanie");

        }

        [HttpPost]
        public void Post([FromBody]Article Article) {
            _repository.AddArticle(Article);
        }

        [HttpPut]
        public void Put([FromBody] Article Article) {
            _repository.UpdateArticle(Article);
        }

        [HttpDelete("{id}")]
        public void Delete(int id) {
            _repository.DeleteArticle(id);
        }
    }
}